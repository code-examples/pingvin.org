# [pingvin.org](https://pingvin.org) source codes


<br/>

### Run pingvin.org on localhost

    # vi /etc/systemd/system/pingvin.org.service

Insert code from pingvin.org.service
    
    # systemctl enable pingvin.org.service
    # systemctl start  pingvin.org.service
    # systemctl status pingvin.org.service


http://localhost:4023
